FROM php:7.3-fpm

RUN pecl install xdebug \
    && docker-php-ext-enable xdebug

RUN curl -L -o /opt/apm-agent-php.deb https://github.com/elastic/apm-agent-php/releases/download/v1.4.2/apm-agent-php_1.4.2_all.deb \
 && dpkg -i /opt/apm-agent-php.deb
